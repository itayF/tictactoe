import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class TicTacToe extends Applet implements ActionListener
{
    private Button newGameButton;
    private Label score;
    private Button[] squares;
    private String winner;
    private int emptySquaresLeft;
    private Label winL;
    private Label computerWinL;
    private Label tikoL;
    private int tiko, win, computerWin;
    
    public void init()
    {
        this.setLayout(new BorderLayout());
        this.setBackground(Color.GREEN);

        win = 0;
        computerWin = 0;
        tiko = 0;
        winL = new Label("��������� ���:" + win);
        tikoL = new Label("����: " + tiko);
        computerWinL = new Label("������: " + computerWin);
        
        
        Panel p1 = new Panel();
        p1.setLayout(new GridLayout(3, 1));
        p1.add(winL);
        p1.add(tikoL);
        p1.add(computerWinL);
        this.add(p1, "East");
        
        newGameButton = new Button("new game");
        newGameButton.addActionListener(this);
        newGameButton.setEnabled(false);
        Panel top = new Panel();
        top.add(newGameButton);
        this.add(top, "North");

        Panel centerPanel = new Panel();
        centerPanel.setLayout(new GridLayout(3, 3));
        this.add(centerPanel, "Center");

        score = new Label("");
        this.add(score, "South");

        squares = new Button[9];
        for (int i = 0; i < 9; i++)
        {
            squares[i] = new Button();
            squares[i].addActionListener(this);
            squares[i].setBackground(Color.BLUE);
            centerPanel.add(squares[i]);
        }
        emptySquaresLeft = 9;
    }

    public void paint(Graphics g)
    {

    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        Button b = (Button) e.getSource();
        if (b ==newGameButton){
            for(int i=0;i<9;i++){
            squares[i].setEnabled(true);
            squares[i].setLabel("");
            squares[i].setBackground(Color.BLUE);
            }
            emptySquaresLeft=9;
            newGameButton.setEnabled(false);
            return;
        }
        for (int i = 0; i < 9; i++)
        {
            if (b == squares[i])
            {
                squares[i].setLabel("X");
                squares[i].setEnabled(false);
                winner = lookForWinner();

                if (!"".equals(winner))
                {
                    endGame();
                }
                else
                {
                    computerMove();
                    winner = lookForWinner();

                    if (!"".equals(winner))
                    {
                        endGame();
                    }
                }
                break;
            }
        }

        if (winner.equals("X"))
        {
            score.setText("�����");
            win ++;
            winL.setText("��������� ���:" + win);
        }
        else if (winner.equals("O"))
        {
            score.setText("�����");
            computerWin ++;
            computerWinL.setText("������: " + computerWin);
        }
        else if (winner.equals("T"))
        {
            score.setText("����");
            tiko ++;
            tikoL.setText("����: " + tiko);
        }
    }

    /**
     * This method applies a set of rules to find the best computer's move. If a
     * good move can't be found, it picks a random square.
     */
    private void computerMove()
    {
        int selectedSquare;
        // Computer first tries to find an empty
        // square next the two squares with "O" to win
        selectedSquare = findEmptySquare("O");
        // if can't find two "O", at least try to stop the
        // opponent from making 3 in a row by placing
        // "O" next to 2 "X".
        if (selectedSquare == -1)
        {
            selectedSquare = findEmptySquare("X");
        }
        // if the selectedSquare is still -1, at least
        // try to occupy the central square
        if ((selectedSquare == -1)
                && (squares[4].getLabel().equals("")))
        {
            selectedSquare = 4;
        }
        // no luck with the central either...
        // just get a random square
        if (selectedSquare == -1)
        {
            selectedSquare = getRandomSquare();
        }
        squares[selectedSquare].setLabel("O");
        squares[selectedSquare].setEnabled(false);
    }

    private int getRandomSquare()
    {
        boolean gotEmptySquare = false;
        int selectedSquare = -1;
        do
        {
            selectedSquare = (int) (Math.random() * 9);
            if (squares[selectedSquare].getLabel().equals(""))
            {
                gotEmptySquare = true; // to end the loop
            }
        } while (!gotEmptySquare);
        return selectedSquare;
    }

    /**
     * This method checks every row, column and diagonal to see if there are two
     * squares with the same label and an empty square.
     * 
     * @param give
     *            X - for user, and O for computer
     * @return the number of the empty square to use, or the negative 1 could
     *         not find 2 square with the same label
     */
    private int findEmptySquare(String player)
    {
        int weight[] = new int[9];
        for (int i = 0; i < 9; i++)
        {
            if (squares[i].getLabel().equals("O"))
                weight[i] = -1;
            else if (squares[i].getLabel().equals("X"))
                weight[i] = 1;
            else
                weight[i] = 0;
        }
        int twoWeights = player.equals("O") ? -2 : 2;
        // See if row 1 has the same 2 squares and a blank
        if (weight[0] + weight[1] + weight[2] == twoWeights)
        {
            if (weight[0] == 0)
                return 0;
            else if (weight[1] == 0)
                return 1;
            else
                return 2;
        }
        // See if row 2 has the same 2 squares and a blank
        if (weight[3] + weight[4] + weight[5] == twoWeights)
        {
            if (weight[3] == 0)
                return 3;
            else if (weight[4] == 0)
                return 4;
            else
                return 5;
        }
        // See if row 3 has the same 2 squares and a blank
        if (weight[6] + weight[7] + weight[8] == twoWeights)
        {
            if (weight[6] == 0)
                return 6;
            else if (weight[7] == 0)
                return 7;
            else
                return 8;
        }
        // See if column 1 has the same 2 squares and a blank
        if (weight[0] + weight[3] + weight[6] == twoWeights)
        {
            if (weight[0] == 0)
                return 0;
            else if (weight[3] == 0)
                return 3;
            else
                return 6;
        }
        // See if column 2 has the same 2 squares and a blank
        if (weight[1] + weight[4] + weight[7] == twoWeights)
        {
            if (weight[1] == 0)
                return 1;
            else if (weight[4] == 0)
                return 4;
            else
                return 7;
        }
        // See if column 3 has the same 2 squares and a blank
        if (weight[2] + weight[5] + weight[8] == twoWeights)
        {
            if (weight[2] == 0)
                return 2;
            else if (weight[5] == 0)
                return 5;
            else
                return 8;
        }
        // See if diagonal 1 has the same 2 squares and a blank
        if (weight[0] + weight[4] + weight[8] == twoWeights)
        {
            if (weight[0] == 0)
                return 0;
            else if (weight[4] == 0)
                return 4;
            else
                return 8;
        }
        // See if diagonal has the same 2 squares and a blank
        if (weight[2] + weight[4] + weight[6] == twoWeights)
        {
            if (weight[2] == 0)
                return 2;
            else if (weight[4] == 0)
                return 4;
            else
                return 6;
        }
        // There are no two neighbors that are the same
        return -1;
    } // end of findEmptySquare()

    private void endGame()
    {
        newGameButton.setEnabled(true);
        for (int i = 0; i < 9; i++)
        {
            squares[i].setEnabled(false);
        }
    }

    private String lookForWinner()
    {
        String theWinner = "";
        emptySquaresLeft--;

        
        // Check the row 1 - array elements 0,1,2
        if (!squares[0].getLabel().equals("") &&
                squares[0].getLabel().equals(squares[1].getLabel()) &&
                squares[0].getLabel().equals(squares[2].getLabel()))
        {
            theWinner = squares[0].getLabel();
            highlightWinner(0, 1, 2);
            // Check the row 2 - array elements 3,4,5
        }
        else if (!squares[3].getLabel().equals("") &&
                squares[3].getLabel().equals(squares[4].getLabel()) &&
                squares[3].getLabel().equals(squares[5].getLabel()))
        {
            theWinner = squares[3].getLabel();
            highlightWinner(3, 4, 5);
            // Check the row 3 - - array elements 6,7,8
        }
        else if (!squares[6].getLabel().equals("") &&
                squares[6].getLabel().equals(squares[7].getLabel()) &&
                squares[6].getLabel().equals(squares[8].getLabel()))
        {
            theWinner = squares[6].getLabel();
            highlightWinner(6, 7, 8);
            // Check the column 1 - array elements 0,3,6
        }
        else if (!squares[0].getLabel().equals("") &&
                squares[0].getLabel().equals(squares[3].getLabel()) &&
                squares[0].getLabel().equals(squares[6].getLabel()))
        {
            theWinner = squares[0].getLabel();
            highlightWinner(0, 3, 6);
            // Check the column 2 - array elements 1,4,7
        }
        else if (!squares[1].getLabel().equals("") &&
                squares[1].getLabel().equals(squares[4].getLabel()) &&
                squares[1].getLabel().equals(squares[7].getLabel()))
        {
            theWinner = squares[1].getLabel();
            highlightWinner(1, 4, 7);
            // Check the column 3 - array elements 2,5,8
        }
        else if (!squares[2].getLabel().equals("") &&
                squares[2].getLabel().equals(squares[5].getLabel()) &&
                squares[2].getLabel().equals(squares[8].getLabel()))
        {
            theWinner = squares[2].getLabel();
            highlightWinner(2, 5, 8);
            // Check the first diagonal - array elements 0,4,8
        }
        else if (!squares[0].getLabel().equals("") &&
                squares[0].getLabel().equals(squares[4].getLabel()) &&
                squares[0].getLabel().equals(squares[8].getLabel()))
        {
            theWinner = squares[0].getLabel();
            highlightWinner(0, 4, 8);
            // Check the second diagonal - array elements 2,4,6
        }
        else if (!squares[2].getLabel().equals("") &&
                squares[2].getLabel().equals(squares[4].getLabel()) &&
                squares[2].getLabel().equals(squares[6].getLabel()))
        {
            theWinner = squares[2].getLabel();
            highlightWinner(2, 4, 6);
        }
        if (theWinner.equals("") && emptySquaresLeft == 0)
        {
            return "T"; // it's a tie;
        }
        return theWinner;
    }

    private void highlightWinner(int i, int j, int k)
    {
        squares[i].setBackground(Color.ORANGE);
        squares[j].setBackground(Color.ORANGE);
        squares[k].setBackground(Color.ORANGE);

    }
    
    public static void main (String[] args)
    {
        Frame frame = new Frame("���� �����");
        Applet myApplet = new TicTacToe();
        myApplet.init();
        frame.setLayout(new GridLayout(1,0));
        frame.setSize(300,300);
        frame.add(myApplet);
        frame.addWindowListener(new WindowAdapter()
        {
            public void windowClosing(WindowEvent e) 
            {
                System.exit(0);
            }
        });
        frame.setVisible(true);
        frame.setIconImage(Toolkit.getDefaultToolkit().getImage("C:/Users/����/Dropbox/programs/TicTacToe/src/TicTacToe.png"));
    }
}